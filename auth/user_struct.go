package auth

import "net/http"

type UserKeyStr struct {
	AppId      string `json:"app_id"`
	Id         string `json:"id"`
	WxNickname string `json:"wx_nickname"`
	WxAvatar   string `json:"wx_avatar"`
}

type UserResponse struct {
	Code int                    `json:"code"`
	Msg  string                 `json:"msg"`
	Data map[string]interface{} `json:"data"`
}

type ContextStruct struct {
	Request *http.Request
	Writer  http.ResponseWriter
}

type UserTokenStr struct {
	Name          string  `json:"name"`
	Value         string  `json:"value"`
	Expires       int     `json:"expires"`
	CookieExpires float32 `json:"cookie_expires"`
	NeedCost      int     `json:"need_cost"`
}

type AuthParams struct {
	RequestPack string `json:"request_pack"`
	AppId       string `json:"app_id"`
}

type UserPostRequest struct {
	HostPrefix        string `form:"host_prefix" json:"host_prefix"`
	AppId             string `form:"app_id" json:"app_id"`
	RedirectUrl       string `form:"redirect_url" json:"redirect_url"`
	State             string `form:"state" json:"state"`
	RegisterCode      string `form:"register_code" json:"register_code"`
	AuthCode          string `form:"auth_code" json:"auth_code"`
	AppType           int    `form:"app_type" json:"app_type"`
	TtToken           string `form:"tt_token" json:"tt_token"`
	BdToken           string `form:"bd_token" json:"bd_token"`
	FeishuToken       string `form:"feishu_token" json:"feishu_token"`
	EncryptData       string `form:"encrypt_data" json:"encrypt_data"`
	SmallClassToken   string `form:"smallclass_token" json:"smallclass_token"`
	XiaoETongAppToken string `form:"xiaoetong-app-token" json:"xiaoetong-app-token"`
}

type LogoutParams struct {
	RequestPack       string `json:"request_pack"`
	AppId             string `json:"app_id"`
	CustomReferrerUri string `json:"custom_referrer_uri"`
}
