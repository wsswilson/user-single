package auth

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"
)

var logger *log.Logger

func init() {
	// 设置app目录
	AppDir, _ := os.Getwd()
	//fmt.Println("AppDir===", AppDir)
	logFileName := filepath.Join(AppDir, "fail.log")
	//logF := "./fail.log"
	f, err := os.OpenFile(logFileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		fmt.Println("打开log文件失败:", err)
		return
	}
	logger = log.New(f, "", log.LUTC)
}

func WriteLog(category string, details string) {
	t := time.Now()
	logger.Printf("%s>>【%s】%s", t.Format(TimeLayout), category, details)
}
