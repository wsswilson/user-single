package auth

const (
	//这里的 code 不从 1 顺序开始的原因是其他历史系统对这个编码有依赖
	ResponseCodeInit         = -2       //single 初始化的 code
	ResponseCodePostErr      = -1       //POST请求错误
	ResponseCodeOK           = 0        //ok
	ResponseCodeNoKey        = 1        //没有返回相应字段 或 未知的 code
	ResponseCodeInvalidToken = 19       //无 token 和 token 过期都是用这个编码
	ResponseCodeInvalidAuth  = 12       //无效的 auth code, 这个 auth_code 是我们自己系统产生授权码，而不是微信的那个 code
	ResponseCodeRedirect     = 302      //重定向指示码
	ResponseCodeNeedLogin    = 10103020 //需要登录

	// 时间格式化
	TimeLayout = "2006-01-02 15:04:05"
)
