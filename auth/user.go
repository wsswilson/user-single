package auth

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"
)

var once sync.Once

var single *UserSingle

//用户单例
type UserSingle struct {
	AppId             string
	Log               bool
	Token             map[string]interface{}
	UserKey           UserKeyStr
	CTX               ContextStruct
	PostData          map[string]interface{}
	Response          UserResponse
	Result            UserResponse
	RawResult         string
	RemoteMethodNames map[string]string
	AuthSuccess       bool
}

//初始化
func (s *UserSingle) Init(request *http.Request, writer http.ResponseWriter, appId string, data map[string]interface{}) {
	//init CTX
	s.CTX = ContextStruct{
		Request: request,
		Writer:  writer,
	}
	s.AppId = appId
	// init PostData
	s.PostData = data
	// init Response  Result
	s.Response = UserResponse{Code: ResponseCodeInit}
	s.Result = UserResponse{Code: ResponseCodeInit}
	//init RemoteMethodNames
	s.RemoteMethodNames = map[string]string{
		"auth":   "/account/auth-token",
		"logout": "/account/logout",
		"token":  "/app/create_token",
	}
}

//设置是否文件记录log
func (s *UserSingle) SetLog(status bool) {
	s.Log = status
}

/*
因为本类把 auth 的结果存在对象属性中，所以 auth 方法只会进行一次远程校验，
如果有时候确实需要再重新做一次校验，那可以调用此方法进行整个对象的重置
*/
func (s *UserSingle) Reset() {
	s.AuthSuccess = false
	s.Response = UserResponse{Code: ResponseCodeInit}
	s.Result = UserResponse{Code: ResponseCodeInit}
	s.RawResult = ""
	s.UserKey = UserKeyStr{}
}

func (s *UserSingle) GetResponse() UserResponse {
	return s.Response
}

func (s *UserSingle) GetServiceAddress(url, methodName string) string {
	if url[len(url)-1:] == "/" {
		url = url[:len(url)-1]
	}
	url = fmt.Sprintf("%s%s", url, s.RemoteMethodNames[methodName])
	return url
}

/*
因为这是个同步的远程请求，所以如果 auth 成功需要做其他事情，那调用方直接在执行 auth 的下一步做后续事情即可，因此就不增加一个类似于 successCallback 这样的参数了
但是 $customReferrerUri 这个参数是必须的，这个参数可能是跳到别的服务之后，那个服务再根据这个参数进行跳转，因此，这个参数可以是一个用于302的url，也可以是把多种信息打包到一起后的序列化字符串
return bool
*/
func (s *UserSingle) Auth(keepOnlineUrl, customReferrerUri string) bool {
	if s.QuickLoginCheck() {
		s.AuthSuccess = true
		return s.AuthSuccess
	}

	needLog := true
	params := map[string]string{}
	appId := s.AppId
	if appId == "" {
		s.AuthSuccess = false
		//appId = s.getDataFromRequestData("app_id")
		return s.AuthSuccess
	}
	params["app_id"] = appId
	if customReferrerUri != "" {
		params["custom_referrer_uri"] = customReferrerUri
	}
	params["request_pack"] = s.RequestEncoder()

	KoUrl := s.GetServiceAddress(keepOnlineUrl, "auth")
	s.Result, s.RawResult = PostToKO(KoUrl, params)
	//fmt.Println("s.Result:", s.Result)
	fmt.Printf("Auth s.Result:%+v\n", s.Result)
	if s.Result.Code == ResponseCodeInit {
		s.SetResponseAndAuth(ResponseCodeNoKey, "result 或 result[code] 为空", false)
	} else if s.Result.Code == ResponseCodePostErr {
		s.SetResponseAndAuth(ResponseCodePostErr, "PostToKeepOnline()内部错误", false)
	} else if s.Result.Code == ResponseCodeInvalidToken {
		s.SetResponseAndAuth(ResponseCodeInvalidToken, "invalid access token", false)
	} else if s.Result.Code == ResponseCodeNeedLogin {
		s.SetResponseAndAuth(ResponseCodeNeedLogin, "need login", false)
	} else if s.Result.Code == ResponseCodeInvalidAuth {
		s.SetResponseAndAuth(ResponseCodeInvalidAuth, "invalid auth code", false)
	} else if s.Result.Code == ResponseCodeRedirect {
		urlR, isOk := s.Result.Data["url"]
		if isOk && urlR != "" {
			needLog = false
			urlStr := fmt.Sprintf("%v", urlR)
			s.SetResponseAndAuth(ResponseCodeRedirect, urlStr, false)
		} else {
			s.SetResponseAndAuth(ResponseCodeNoKey, "resule[data][url] 为空，无法重定向", false)
		}
	} else if s.Result.Code == ResponseCodeOK {
		token, isToken := s.Result.Data["token"]
		user, isUser := s.Result.Data["user"]
		if isToken == false || isUser == false {
			s.SetResponseAndAuth(ResponseCodeNoKey, "resule[data][token|user] 为空", false)
		} else {
			redirect, isRedirect := s.Result.Data["redirect"]
			if isRedirect && redirect != "" {
				redirectStr := fmt.Sprintf("%v", redirect)
				s.SetResponse(ResponseCodeRedirect, redirectStr)
			}
			s.Token = token.(map[string]interface{})
			s.SetUserKey(user.(map[string]string))
			s.SetResponse(ResponseCodeOK, "ok")
			s.AuthSuccess = true
		}
	} else {
		s.SetResponseAndAuth(ResponseCodeNoKey, "未做特殊处理的 result[code]", false)
	}
	if s.AuthSuccess == false {
		s.LogFail(needLog)
	}
	return s.AuthSuccess
}

//设置 Response
func (s *UserSingle) SetResponse(code int, msg string) {
	if code == 302 {
		s.Response = UserResponse{
			Code: code,
			Data: map[string]interface{}{"url": msg},
		}
	} else {
		s.Response = UserResponse{
			Code: code,
			Msg:  msg,
		}
	}
}

func (s *UserSingle) SetResponseAndAuth(code int, msg string, auth bool) {
	s.SetResponse(code, msg)
	s.AuthSuccess = auth
}

func (s *UserSingle) RequestEncoder() string {
	host := s.CTX.Request.Host
	urlR := s.CTX.Request.URL
	method := s.CTX.Request.Method
	//header := s.CTX.Request.Header
	userAgent := s.CTX.Request.UserAgent()
	cookies := s.CTX.Request.Cookies()
	temp := map[string]interface{}{}
	temp["uri"] = fmt.Sprintf("%s%s", host, urlR)
	temp["method"] = method
	temp["post"] = s.PostData
	temp["cookie"] = cookies
	temp["server"] = map[string]string{"HTTP_USER_AGENT": userAgent}
	temp["content"] = ""
	//fmt.Println("temp:", temp)
	jsonMap, _ := json.Marshal(temp)
	str := base64.StdEncoding.EncodeToString(jsonMap)
	return str
}

//批量设置 cookie 键值
func (s *UserSingle) SetCookieValues(cookieMap map[string]string) {
	var err error
	for k, v := range cookieMap {
		_, err = s.GetCookie(k)
		if err != nil {
			s.SetCookie(k, v, 0, "", "", false, false)
		}
	}
}

func (s *UserSingle) GetCookie(name string) (string, error) {
	cookie, err := s.CTX.Request.Cookie(name)
	if err != nil {
		return "", err
	}
	val, _ := url.QueryUnescape(cookie.Value)
	return val, nil
}

//设置 cookie 键值
func (s *UserSingle) SetCookie(name, value string, maxAge int, path, domain string, secure, httpOnly bool) {
	if path == "" {
		path = "/"
	}
	http.SetCookie(s.CTX.Writer, &http.Cookie{
		Name:     name,
		Value:    url.QueryEscape(value),
		MaxAge:   maxAge,
		Path:     path,
		Domain:   domain,
		SameSite: http.SameSiteDefaultMode,
		Secure:   secure,
		HttpOnly: httpOnly,
	})
}

// cookies 内是否全部包含 app_id user_id avatar nickname 四个字段
func (s *UserSingle) UserMsgInCookies() bool {
	var err error
	_, err = s.GetCookie("app_id")
	if err != nil {
		return false
	}
	_, err = s.GetCookie("user_id")
	if err != nil {
		return false
	}
	_, err = s.GetCookie("avatar")
	if err != nil {
		return false
	}
	_, err = s.GetCookie("nickname")
	if err != nil {
		return false
	}
	return true
}

//取appid，慎重
func (s *UserSingle) getAppIdFromHost() string {
	envEnd := "." + "h5.inside.xiaoeknow.com"
	host := s.CTX.Request.Host
	end := strings.Index(host, ".com") + 4
	host = fmt.Sprintf("%s/", host[:end])
	host = strings.Replace(host, "http://", "", 1)
	host = strings.Replace(host, "https://", "", 1)
	endIndex := strings.Index(host, envEnd)
	if endIndex > 0 {
		return host[0:endIndex]
	} else {
		return ""
	}
}

// 从请求对象中获取数据 getDataFromRequestData
func (s *UserSingle) getDataFromRequestData(key string) string {
	queryParam := s.CTX.Request.URL.Query()
	value := queryParam.Get(key)
	if value == "" {
		value, _ = s.GetCookie(key)
	}
	if value == "" {
		return ""
	} else {
		return value
	}
}

func (s *UserSingle) QuickLoginCheck() bool {
	queryParam := s.CTX.Request.URL.Query()
	appId := queryParam.Get("app_id")
	userId := queryParam.Get("user_id")
	avatar := queryParam.Get("avatar")
	nickname := queryParam.Get("nickname")
	//首页参数会在链接中，非首页需要从cookie中取
	if s.UserMsgInCookies() {
		appId, _ = s.GetCookie("app_id")
		userId, _ = s.GetCookie("user_id")
		avatar, _ = s.GetCookie("avatar")
		nickname, _ = s.GetCookie("nickname")
	}

	if appId == "" || userId == "" {
		return false
	}
	//不是以a01开头的非智慧校园用户不允许快捷登录
	if appId[0:3] != "a01" {
		return false
	}

	s.UserKey = UserKeyStr{
		AppId:      appId,
		Id:         userId,
		WxNickname: nickname,
		WxAvatar:   avatar,
	}

	d := map[string]string{"app_id": appId, "user_id": userId, "avatar": avatar, "nickname": nickname}
	s.SetCookieValues(d)

	return true
}

func (s *UserSingle) QuickLogoutCheck() bool {

	//首页参数会在链接中，非首页需要从cookie中取
	if s.UserMsgInCookies() {
		appId, _ := s.GetCookie("app_id")
		//不是以a01开头的非智慧校园用户不允许快捷登录
		if appId[0:3] != "a01" {
			return false
		} else {
			tNow := time.Now().Unix()
			t1 := int(tNow - 3600*24*30)
			s.SetCookie("app_id", "", t1, "", "", false, false)
			s.SetCookie("user_id", "", t1, "", "", false, false)
			s.SetCookie("avatar", "", t1, "", "", false, false)
			s.SetCookie("nickname", "", t1, "", "", false, false)
			return true
		}
	}
	return false
}

func (s *UserSingle) LogFail(needLog bool) {
	if s.Log == false || needLog == false {
		return
	}
	jsonMap, _ := json.Marshal(s.Result)
	WriteLog("raw-remote-result", s.RawResult)
	WriteLog("local-response", string(jsonMap))
}

/*
因为这是个同步的远程请求，所以如果 logout 成功需要做其他事情，那调用方直接在执行 logout 的下一步做后续事情即可，因此就不增加一个类似于 successCallback 这样的参数了
但是 $customReferrerUri 这个参数是必须的，这个参数可能是跳到别的服务之后，那个服务再根据这个参数进行跳转，因此，这个参数可以是一个用于302的url，也可以是把多种信息打包到一起后的序列化字符串
*/
func (s *UserSingle) Logout(keepOnlineUrl, customReferrerUri string) bool {
	tNow := time.Now().Unix()
	t1 := int(tNow - 3600)
	s.SetCookie("logintime", "", t1, "/", "", false, false)

	if s.QuickLogoutCheck() {
		return true
	}

	params := map[string]string{}
	appId := s.AppId
	params["app_id"] = appId
	if customReferrerUri != "" {
		params["custom_referrer_uri"] = customReferrerUri
	}
	params["request_pack"] = s.RequestEncoder()
	KoUrl := s.GetServiceAddress(keepOnlineUrl, "logout")
	s.Result, s.RawResult = PostToKO(KoUrl, params)
	if s.Result.Code == ResponseCodeInit {
		s.LogFail(true)
		s.SetResponse(ResponseCodeNoKey, "result 或 resule[code] 为空")
		return false
	}
	if s.Result.Code == ResponseCodePostErr {
		s.SetResponse(ResponseCodePostErr, "PostToKeepOnline()内部错误")
		return false
	}
	if s.Result.Code == ResponseCodeOK {
		urlR, isOk := s.Result.Data["url"]
		if isOk && urlR != "" {
			urlStr := fmt.Sprintf("%v", urlR)
			s.SetResponse(ResponseCodeRedirect, urlStr)
		}
		return true
	}
	s.SetResponse(ResponseCodeNoKey, "未知的 result[code]")
	s.LogFail(true)
	return false
}

func (s *UserSingle) GetTokenByUser(keepOnlineUrl, storeId, storeUserId string) (*UserSingle, error) {
	var err error
	params := map[string]string{}
	params["app_id"] = storeId
	params["user_id"] = storeUserId
	params["biz_type"] = "pc"
	KoUrl := s.GetServiceAddress(keepOnlineUrl, "token")
	s.Result, s.RawResult = PostToKO(KoUrl, params)
	if s.Result.Code != ResponseCodeOK {
		err = errors.New("远程调用失败")
		u := &UserSingle{}
		return u, err
	}
	token, isToken := s.Result.Data["token"]
	user, isUser := s.Result.Data["user"]
	if isToken == false || isUser == false {
		err = errors.New("resule[data][token|user] 为空")
		u := &UserSingle{}
		return u, err
	} else {
		s.Token = token.(map[string]interface{})
		s.SetUserKey(user.(map[string]string))
		s.AuthSuccess = true
		return s, err
	}
}

func (s *UserSingle) SetUserKey(dataUser map[string]string) {
	s.UserKey = UserKeyStr{
		AppId:      dataUser["app_id"],
		Id:         dataUser["id"],
		WxNickname: dataUser["wx_nickname"],
		WxAvatar:   dataUser["wx_avatar"],
	}
}

func (s *UserSingle) GetUserKey(keepOnlineUrl string) UserKeyStr {
	UK := UserKeyStr{}
	if s.Auth(keepOnlineUrl, "") == true {
		UK = s.UserKey
		return UK
	}
	return UK
}

func (s *UserSingle) GetValOfUserKeyByField(keepOnlineUrl, field string) (val string, err error) {
	UK := s.GetUserKey(keepOnlineUrl)
	if UK.AppId == "" && UK.Id == "" {
		err = errors.New("app_id 和 id 为空")
		return
	}
	UkJson, err := json.Marshal(UK)
	if err != nil {
		return
	}
	var UkMap map[string]string
	err = json.Unmarshal(UkJson, &UkMap)
	if err != nil {
		return
	}
	UkVal, isOk := UkMap[field]
	if isOk {
		val = UkVal
		return
	} else {
		err = errors.New(fmt.Sprintf("UserKey没有 %s 的值", field))
		return
	}
}

func (s *UserSingle) GetTokenName(keepOnlineUrl string) (name string) {
	if s.Auth(keepOnlineUrl, "") == true {
		token, isOk := s.Token["name"]
		if isOk {
			name = token.(string)
			return
		} else {
			return
		}
	}
	return
}

func (s *UserSingle) GetToken(keepOnlineUrl string) (val string) {
	if s.Auth(keepOnlineUrl, "") == true {
		token, isOk := s.Token["value"]
		if isOk {
			val = token.(string)
			return
		} else {
			return
		}
	}
	return
}

func (s *UserSingle) GetTokenExpires() (n int) {
	token, isOk := s.Token["expires"]
	if isOk {
		n = token.(int)
		return
	} else {
		n = -1
		return
	}
}

func (s *UserSingle) GetCookieExpires() (f float32) {
	token, isOk := s.Token["cookie_expires"]
	if isOk {
		f = token.(float32)
		return
	} else {
		f = -1
		return
	}
}

func (s *UserSingle) GetResult() UserResponse {
	return s.Result
}

func (s *UserSingle) GetRawResult() string {
	return s.RawResult
}

func (s *UserSingle) GetTokenExpiresAsTimestamp() (n int) {
	tNow := int(time.Now().Unix())
	n = s.GetTokenExpires() + tNow
	return
}

func (s *UserSingle) GetCookieExpiresAsTimestamp() (f float32) {
	tNow := float32(time.Now().Unix())
	f = s.GetCookieExpires() + tNow
	return
}

func (s *UserSingle) GetNeedCost(keepOnlineUrl string) (cost int) {
	if s.Auth(keepOnlineUrl, "") == true {
		token, isOk := s.Token["need_cost"]
		if isOk {
			cost = token.(int)
			return
		} else {
			return
		}
	}
	return
}

//==========================================================================================

func UserInstance(request *http.Request, writer http.ResponseWriter, appId string, dataWithJsonOrForm map[string]interface{}) *UserSingle {
	once.Do(func() {
		//fmt.Printf("once.Do req:%+v\n", req)
		single = &UserSingle{}
		single.Init(request, writer, appId, dataWithJsonOrForm)
	})
	return single
}

//发送POST请求
func PostToKO(url string, data interface{}) (result UserResponse, rawResult string) {
	jsonStr, _ := json.Marshal(data)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	if err != nil {
		//panic(err)
		result.Code = -1
		result.Msg = err.Error()
		return
	}
	req.Header.Add("content-type", "application/json")

	defer req.Body.Close()

	client := &http.Client{Timeout: 5 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
		result.Code = -1
		result.Msg = err.Error()
		return
	}
	defer resp.Body.Close()

	response, _ := ioutil.ReadAll(resp.Body)
	result = UserResponse{Code: ResponseCodeInit}
	err = json.Unmarshal(response, &result)
	if err != nil {
		result.Code = -1
		result.Msg = err.Error()
		return
	}
	rawResult = string(response)
	return
}
